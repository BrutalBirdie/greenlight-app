# Changelog

## [0.0.6] - 2020-09-02

- Changed default admin E-Mail to admin@server.local
- Added automated tests for admin login
- Cleanup test.js file
- Updated Docker cmd script
- Updated POSTINSTALL.md

## [0.0.7] - 2020-09-02

- Changed default Icon

## [0.0.8] - 2020-10-06

- Changed `/app/data/tmp/` location to a symlink to `/tmp/`
