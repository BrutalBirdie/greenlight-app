FROM cloudron/base:2.0.0

ARG RAILS_ROOT=/app/code/

ENV RAILS_ENV=production
ENV BUNDLE_APP_CONFIG="$RAILS_ROOT/.bundle"
ENV GRENNLIGHTVERSION=2.7.2

RUN mkdir -p $RAILS_ROOT
WORKDIR $RAILS_ROOT

# Make /root a symlink and writeable
RUN rm -rf /root/ &&ln -s /app/data/root /root

# Install needed packages and install bundler
RUN apt-get update && apt-get install tzdata -y  && rm -rf /var/cache/apt /var/lib/apt/lists && gem install bundler

RUN wget https://github.com/bigbluebutton/greenlight/archive/release-${GRENNLIGHTVERSION}.tar.gz \
     && tar xfv release-${GRENNLIGHTVERSION}.tar.gz -C /app/code/ \
     && mv /app/code/greenlight-release-${GRENNLIGHTVERSION}/* /app/code/ \
     && rm -r /app/code/greenlight-release-${GRENNLIGHTVERSION}/ \
     && rm release-${GRENNLIGHTVERSION}.tar.gz

RUN bundle config --global frozen 1 \
    && bundle install --deployment --without development:test:assets -j4 --path=vendor/bundle \
#    && rm -rf vendor/bundle/ruby/2.5.0/cache/*.gem \
    && find vendor/bundle/ruby/2.5.0/gems/ -name "*.c" -delete \
    && find vendor/bundle/ruby/2.5.0/gems/ -name "*.o" -delete 

COPY sample.env create_admin_user.sh gen_secret_key.sh get_secret_key.sh /app/code/

# RUN bundle pristine

RUN rm -rf /app/code/tmp /app/code/log \
    && ln -s /tmp/ /app/code/tmp \
    && ln -s /app/data/log/ /app/code/log \
    && ln -s /app/data/yarn-error.log /app/code/yarn-error.log \
    && mv /app/code/yarn.lock /app/code/yarn.lock_stale \
    && mv /app/code/public /app/code/public_stale \
    && mv /app/code/app/views/main/ /app/code/app/views/main_stale \
    && mv /app/code/config/locales/ /app/code/config/locales_stale \
    && ln -s /app/data/locales /app/code/config/locales \
    && ln -s /app/data/views/ /app/code/app/views/main \
    && ln -s /app/data/public/ /app/code/public \
    && ln -s /app/data/yarn.lock /app/code/yarn.lock \
    && ln -s /app/data/.yarnrc /usr/local/share/.yarnrc \
    && ln -s /app/data/node_modules /app/code/node_modules \
    && echo 'export $(grep -v '^#' /app/data/.env | xargs)' >> /etc/bash.bashrc

COPY start.sh /app/code/bin/start

# Start the application.
CMD ["bin/start"]