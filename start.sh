#!/bin/bash
set -x

# GzEvD copy sample.env to /app/data/.env if not exsists
if [[ ! -f /app/data/.env ]]; then 
    cp -v /app/code/sample.env /app/data/.env
fi

# GzEvD create needed folders for symlinks
if [[ ! -d /app/data/log ]]; then 
    mkdir -p /app/data/log
fi

if [[ ! -f /app/data/yarn.lock ]]; then 
  cp -v /app/code/yarn.lock_stale /app/data/yarn.lock
fi

if [[ ! -d /app/data/node_modules ]]; then
    mkdir -p /app/data/node_modules
fi

if [[ ! -d /app/data/views ]]; then
    cp -rv /app/code/app/views/main_stale /app/data/views
fi

if [[ ! -d /app/data/locales ]]; then
    cp -rv /app/code/config/locales_stale /app/data/locales
fi

if [[ ! -d /app/data/public ]]; then
    cp -rv /app/code/public_stale /app/data/public
fi

if [[ ! -d /app/data/root ]]; then
    mkdir -p /app/data/root
fi 
# END GzEvD

touch /app/data/yarn-error.log
touch /app/data/yarn.lock
touch /app/data/.yarnrc


SECRET_KEY_BASE="$(bundle exec rake secret)" \
&& export SECRET_KEY_BASE \
&& export RAILS_ENV=production SECRET_KEY_BASE=$SECRET_KEY_BASE

sed -i -e "s/SECRET_KEY_BASE=.*/SECRET_KEY_BASE=$SECRET_KEY_BASE/g" /app/data/.env \
&& sed -i -e "s/DB_HOST=.*/DB_HOST=$CLOUDRON_POSTGRESQL_HOST/g" /app/data/.env \
&& sed -i -e "s/DB_PORT=.*/DB_PORT=$CLOUDRON_POSTGRESQL_PORT/g" /app/data/.env \
&& sed -i -e "s/DB_NAME=.*/DB_NAME=$CLOUDRON_POSTGRESQL_DATABASE/g" /app/data/.env \
&& sed -i -e "s/DB_USERNAME=.*/DB_USERNAME=$CLOUDRON_POSTGRESQL_USERNAME/g" /app/data/.env \
&& sed -i -e "s/DB_PASSWORD=.*/DB_PASSWORD=$CLOUDRON_POSTGRESQL_PASSWORD/g" /app/data/.env

sed -i -e "s/LDAP_SERVER=.*/LDAP_SERVER=$CLOUDRON_LDAP_SERVER/g" /app/data/.env \
&& sed -i -e "s/LDAP_PORT=.*/LDAP_PORT=$CLOUDRON_LDAP_PORT/g" /app/data/.env \
&& sed -i -e "s/LDAP_BASE=.*/LDAP_BASE=$CLOUDRON_LDAP_USERS_BASE_DN/g" /app/data/.env \
&& sed -i -e "s/LDAP_BIND_DN=.*/LDAP_BIND_DN=$CLOUDRON_LDAP_BIND_DN/g" /app/data/.env \
&& sed -i -e "s/LDAP_PASSWORD=.*/LDAP_PASSWORD=$CLOUDRON_LDAP_BIND_PASSWORD=/g" /app/data/.env \
&& sed -i -e "s/LDAP_METHOD=.*/LDAP_METHOD=plain/g" /app/data/.env \
&& sed -i -e "s/LDAP_UID=.*/LDAP_UID=username/g" /app/data/.env \
&& sed -i -e "s/LDAP_AUTH=.*/LDAP_AUTH=simple/g" /app/data/.env \
&& sed -i -e "s/LDAP_ROLE_FIELD=.*/LDAP_ROLE_FIELD=users/g" /app/data/.env \
&& sed -i -e "s/LDAP_FILTER=.*/LDAP_FILTER=/g" /app/data/.env \
&& sed -i -e "s/LDAP_ATTRIBUTE_MAPPING=.*/LDAP_ATTRIBUTE_MAPPING=uid=uid;name=displayname;email=mail;nickname=givenName;/g" /app/data/.env

sed -i -e "s/SMTP_SERVER=.*/SMTP_SERVER=$CLOUDRON_MAIL_SMTP_SERVER/g" /app/data/.env \
&& sed -i -e "s/SMTP_PORT=.*/SMTP_PORT=$CLOUDRON_MAIL_SMTP_PORT/g" /app/data/.env \
&& sed -i -e "s/SMTP_DOMAIN=.*/SMTP_DOMAIN=$CLOUDRON_MAIL_DOMAIN/g" /app/data/.env \
&& sed -i -e "s/SMTP_USERNAME=.*/SMTP_USERNAME=$CLOUDRON_MAIL_SMTP_USERNAME/g" /app/data/.env \
&& sed -i -e "s/SMTP_PASSWORD=.*/SMTP_PASSWORD=$CLOUDRON_MAIL_SMTP_PASSWORD/g" /app/data/.env \
&& sed -i -e "s/SMTP_AUTH=.*/SMTP_AUTH=user/g" /app/data/.env \
&& sed -i -e "s/SMTP_STARTTLS_AUTO=.*/SMTP_STARTTLS_AUTO=true/g" /app/data/.env \
&& sed -i -e "s/SMTP_SENDER=.*/SMTP_SENDER=$CLOUDRON_MAIL_FROM/g" /app/data/.env

source /app/data/.env

# Export all varaibles in /app/data/.env
echo 'Reading varaibles in /app/data/.env'
export $(grep -v '^#' /app/data/.env | xargs)

if [ "$RAILS_ENV" = "production" ] && [ "$DB_ADAPTER" = "postgresql" ]; then
  while ! curl http://$CLOUDRON_POSTGRESQL_HOST:${CLOUDRON_POSTGRESQL_PORT:-5432}/ 2>&1 | grep '52'
  do
    echo "Waiting for postgres to start up ..."
    sleep 1
  done
fi

db_create="$(RAILS_ENV=$RAILS_ENV bundle exec rake db:create 2>&1)"
echo $db_create

if [[ $db_create == *"already exists"* ]]; then
  echo ">>> Database migration"
  bundle exec rake db:migrate
else
  echo ">>> Database initialization"
  bundle exec rake db:schema:load
fi

bundle exec --verbose rake assets:precompile --trace --verbose

# Create default admin user

bundle exec rake user:create["admin","admin@server.local","changeme","admin"]

# Start Greenlight
exec bundle exec puma -C config/puma.rb