#!/bin/bash

cd /app/code/
export $(grep -v '^#' /app/data/.env | xargs)

echo "Please give a Username: "
read name
echo "Please give a E-Mail: "
read email
echo "Please give a password: "
read password

echo "Is this correct? (y/n)"
echo $name $email $password
read answer

if [[ "$answer" == "y" ]]
then
    SECRET_KEY_BASE="$(grep -ri "SECRET_KEY_BASE=" /app/data/.env | sed 's/.*=//')"
    export SECRET_KEY_BASE
    export RAILS_ENV=production SECRET_KEY_BASE=$SECRET_KEY_BASE
    bundle exec rake user:create["$name","$email","$password","admin"]
else
    echo "Please restart this tool and give the correct information."
fi
