#!/usr/bin/env node

'use strict';

/* global describe */
/* global after */
/* global before */
/* global it */

require('chromedriver');

var execSync = require('child_process').execSync,
    expect = require('expect.js'),
    path = require('path'),
    { Builder, By, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

if (!process.env.USERNAME || !process.env.PASSWORD) {
    console.log('USERNAME and PASSWORD env vars need to be set');
    process.exit(1);
}

describe('Application life cycle test', function () {
    this.timeout(0);

    var TEST_TIMEOUT = 20000;
    var EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };
    var LOCATION = 'test';

    var app;
    var username = process.env.USERNAME;
    var password = process.env.PASSWORD;
    var adminEmail = 'admin@server.local';
    var adminPassword = 'changeme';
    var server, browser = new Builder().forBrowser('chrome').build();

    before(function () {
        var seleniumJar= require('selenium-server-standalone-jar');
        var SeleniumServer = require('selenium-webdriver/remote').SeleniumServer;
        server = new SeleniumServer(seleniumJar.path, { port: 4444 });
        server.start();
    });

    after(function () {
        browser.quit();
    });

    function getAppInfo() {
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION; })[0];
        expect(app).to.be.an('object');
    }

    function waitForElement(elem) {
        return browser.wait(until.elementLocated(elem), TEST_TIMEOUT).then(function () {
            return browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
        });
    }

    function ldap_login(username, password, callback) {
        browser.get(`https://${app.fqdn}/ldap_signin`).then(function () {
            return browser.wait(until.elementLocated(By.id('session_username')), TEST_TIMEOUT);
        }).then(function () {
            return browser.wait(until.elementIsVisible(browser.findElement(By.id('session_username'))), TEST_TIMEOUT);
        }).then(function () {
            return browser.findElement(By.id('session_username')).sendKeys(username);
        }).then(function () {
            return browser.findElement(By.id('session_password')).sendKeys(password);
        }).then(function () {
            return browser.findElement(By.xpath('/html/body/div[2]/div[2]/div/div/div/div[2]/form/div[3]/input')).submit();
        }).then(function () {
            callback();
        });
    }

    function login(username, password, callback) {
        browser.get(`https://${app.fqdn}/signin`).then(function () {
            return browser.wait(until.elementLocated(By.id('session_email')), TEST_TIMEOUT);
        }).then(function () {
            return browser.wait(until.elementIsVisible(browser.findElement(By.id('session_email'))), TEST_TIMEOUT);
        }).then(function () {
            return browser.findElement(By.id('session_email')).sendKeys(username);
        }).then(function () {
            return browser.findElement(By.id('session_password')).sendKeys(password);
        }).then(function () {
            return browser.findElement(By.xpath('/html/body/div[2]/div/div/div/div/div[2]/form/div[3]/input')).submit();
        }).then(function () {
            callback();
        });
    }

    function logout(callback) {
        browser.get(`https://${app.fqdn}`);
        waitForElement(By.xpath('/html/body/div[1]/div/div/div/div/a')).then(function () {
            return browser.findElement(By.xpath('/html/body/div[1]/div/div/div/div/a')).click();
        }).then(function () {
            return browser.sleep(1000);
        }).then(function () {
            return waitForElement(By.xpath('/html/body/div[1]/div/div/div/div/div/form/button'));
        }).then(function () {
            return browser.findElement(By.xpath('/html/body/div[1]/div/div/div/div/div/form/button')).click();
        }).then(function () {
            return browser.wait(until.elementLocated(By.xpath('/html/body/div[1]/div/div/div/a[1]')), TEST_TIMEOUT);
        }).then(function () {
            callback();
        });
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });
    it('install app', function () { execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS); });
    it('can get app information', getAppInfo);

    it('can ldap_login', ldap_login.bind(null, username, password));
    it('can logout', logout);
    it('can login', login.bind(null, adminEmail, adminPassword));
    it('can logout', logout);
    it('can restart app', function () { execSync(`cloudron restart --app ${app.id}`); });

    it('can ldap_login', ldap_login.bind(null, username, password));
    it('can logout', logout);
    it('can login', login.bind(null, adminEmail, adminPassword));
    it('can logout', logout);

    it('backup app', function () { execSync(`cloudron backup create --app ${app.id}`); });
    it('restore app', function () {
        const backups = JSON.parse(execSync('cloudron backup list --raw'));
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
        execSync('cloudron install --location ' + LOCATION, EXEC_ARGS);
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION; })[0];
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    });

    it('can ldap_login', ldap_login.bind(null, username, password));
    it('can logout', logout);
    it('can login', login.bind(null, adminEmail, adminPassword));
    it('can logout', logout);

    it('move to different location', function (done) {
        browser.manage().deleteAllCookies();

        // ensure we don't hit NXDOMAIN in the mean time
        browser.get('about:blank').then(function () {
            execSync(`cloudron configure --app ${app.id} --location ${LOCATION}2`, EXEC_ARGS);

            var inspect = JSON.parse(execSync('cloudron inspect'));
            app = inspect.apps.filter(function (a) { return a.location === LOCATION + '2'; })[0];
            expect(app).to.be.an('object');

            done();
        });
    });

    it('can ldap_login', ldap_login.bind(null, username, password));
    it('can logout', logout);
    it('can login', login.bind(null, adminEmail, adminPassword));
    it('can logout', logout);

    it('uninstall app', function (done) {
        // ensure we don't hit NXDOMAIN in the mean time
        browser.get('about:blank').then(function () {
            execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
            done();
        });
    });

    // // test update
    // it('can install app', function () { execSync(`cloudron install --appstore-id com.nextcloud.cloudronapp --location ${LOCATION}`, EXEC_ARGS); });
    // it('can get app information', getAppInfo);

    // it('can login', login.bind(null, username, password));
    // it('can logout', logout);

    // it('can update', function () { execSync(`cloudron update --app ${LOCATION}`, EXEC_ARGS); });

    // it('uninstall app', function (done) {
    //     // ensure we don't hit NXDOMAIN in the mean time
    //     browser.get('about:blank').then(function () {
    //         execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    //         done();
    //     });
    // });
});
